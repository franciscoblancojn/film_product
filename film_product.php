<?php
/*
Plugin Name: Film Product
Plugin URI: https://gitlab.com/franciscoblancojn/film_product
Description: Permite agregar peliculas a productos
Author: Francisco Blanco
Version: 1.8.6
Author URI: https://franciscoblanco.vercel.app/
License: 
 */

define("FIPR",plugin_dir_path( __FILE__ ));

require_once FIPR . 'src/admin.php';